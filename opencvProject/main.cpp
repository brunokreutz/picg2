//  g++ main.cpp -o main `pkg-config --cflags --libs opencv`
//  main.cpp
//  opencvProject
//
//  Created by Bruno Kreutz e Rodolfo on 2015-02-02.
//  Copyright (c) 2015 Bruno Kreutz e Rodolfo. All rights reserved.

#include <math.h>
#include <iostream>
#include <string>
#include <fstream>
#include <random>
#include <stdlib.h>
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

string op;
string nomeSaida;
IplImage* imgIn;
IplImage* imgOut;

void carregarImg(char* argv[], int posicao, int qtdArg){
    if (posicao < qtdArg) {
        imgIn = cvLoadImage(argv[posicao], CV_LOAD_IMAGE_GRAYSCALE);
        if (imgIn == NULL) {
            cout << " -- Imagem de entrada invalida" << endl;
            exit(0);
        }
    }else{
        cout << " -- Imagem de entrada nao informada" << endl;
        exit(0);
    }
}

void lerNomeSaida(char* argv[], int posicao, int qtdArg){
    if (posicao < qtdArg) {
        nomeSaida = argv[posicao];
    }else{
        cout << " -- Nome arquivo de saida nao informado" << endl;
        exit(0);
    }
}


void criarImgSaida(int op, int tam = 0){
    cout<< tam;
    int t = tam-1;
    switch (op) {
        case 0:
            imgOut = cvCreateImage(cvGetSize(imgIn), IPL_DEPTH_8U, 1);
            break;
            //Histograma
        case 1:
            imgOut = cvCreateImage(cvSize(256, 256), IPL_DEPTH_8U, 1);
            break;
            //Sobel
        case 2:
            imgOut = cvCreateImage(cvSize(imgIn->width-2, imgIn->height-2), IPL_DEPTH_8U, 1);
            break;
            //Gaussiana
        case 3:
            imgOut = cvCreateImage(cvSize(imgIn->width-t, imgIn->height-t), IPL_DEPTH_8U, 1);
            break;
    }
}


void negativo(IplImage* img, IplImage* out){
    for( int y=0; y<img->height; y++ ) {
        uchar* ptr_img = (uchar*) (img->imageData + y * img->widthStep);
        uchar* ptr_out = (uchar*) (out->imageData + y * out->widthStep);
        
        for( int x=0; x<img->width; x++ ) {
            ptr_out[x] = 255-ptr_img[x];
        }
    }
    
}

void validarNegativo(int argc, char* argv []){
    carregarImg(argv, 2, argc);
    lerNomeSaida(argv, 3, argc);
    criarImgSaida(0);
}

void limiarizar(IplImage* img, IplImage* out, int limiar){
    for( int y=0; y<img->height; y++ ) {
        uchar* ptr_img = (uchar*) (img->imageData + y * img->widthStep);
        uchar* ptr_out = (uchar*) (out->imageData + y * out->widthStep);
        
        for( int x = 0; x < img->width; x++ ) {
            uchar cor = ptr_img[x];
            if(cor<limiar){
                ptr_out[x] = 0;
            }else{
                ptr_out[x] = 255;
            }
        }
    }
}

int validarLimiarizacao(int argc, char* argv []){
    carregarImg(argv, 2, argc);
    lerNomeSaida(argv, 3, argc);
    criarImgSaida(0);
    if (4 < argc) {
        string lim = argv[4];
        int limiar = atoi(lim.c_str());
        if (limiar > 0 && limiar < 255) {
            return limiar;
        }else{
            cout << " -- Valor invalido de limiar" << endl;
            exit(0);
        }
    }else{
        cout << " -- Valor nao informado de limiar" << endl;
        exit(0);
    }
}

void histograma(IplImage* img, IplImage* out, ofstream &outfile){
    int hist[255] = {0};
    float histN[255] = {0};
    if(outfile.is_open()){
        for( int y=0; y<img->height; y++ ) {
            uchar* ptr_img = (uchar*) (img->imageData + y * img->widthStep);
            for( int x=0; x < img->width; x++ ) {
                uchar cor = ptr_img[x];
                hist[cor]++;
            }
        }
        int max = 0;
        for (int i= 0; i < 256; i++){
            outfile << "Intensidade: " << i << " numero de pixels: "<< hist[i] << std::endl;
            if (hist[i]>max){
                max = hist[i];
            }
        }
        for (int i=0; i<256; i++){
            histN[i] = round(float(float(hist[i])/float(max))*float(255));
        }
        
        
        for (int k = 0; k < out->height; k++){
            uchar* ptr_out = (uchar*)(out->imageData + k * out->widthStep);
            for (int l = 0; l < out->width; l++){
                if (255-histN[l] >k)
                    ptr_out[l] = 0;
                else
                    ptr_out[l] = 255;
                
            }
        }
    }
}

string validarHistograma(int argc, char* argv []){
    carregarImg(argv, 2, argc);
    lerNomeSaida(argv, 3, argc);
    criarImgSaida(1);
    if (4 < argc){
        string nomeTexto = argv[4];
        return nomeTexto;
    }
    else{
        cout << " -- Nome arquivo texto nao informado" << endl;
        exit(0);
    }
}

void equalizacao(IplImage* img, IplImage* out){
    int hist[255] = {0};
    float histN[255] = {0};
    
    for( int y=0; y<img->height; y++ ) {
        uchar* ptr_img = (uchar*) (img->imageData + y * img->widthStep);
        for( int x = 0; x < img->width; x++) {
            uchar cor = ptr_img[x];
            hist[cor] = hist[cor] + 1;
        }
    }
    
    float qtdPixels = img->height * img->width;
    
    for (int i= 0; i < 256; i++){
        if (i == 0)
            histN[i] = hist[i]/qtdPixels;
        else
            histN[i] = (hist[i]/qtdPixels) + histN[i-1];
    }
    
    for( int y=0; y<img->height; y++ ) {
        uchar* ptr_img = (uchar*) (img->imageData + y * img->widthStep);
        uchar* ptr_out = (uchar*) (out->imageData + y * out->widthStep);
        for( int x=0; x < img->width; x++ ) {
            uchar cor = ptr_img[x];
            ptr_out[x] = round(histN[cor]*255);
        }
    }
}

void validarEqualizacao(int argc, char* argv []){
    carregarImg(argv, 2, argc);
    lerNomeSaida(argv, 3, argc);
    criarImgSaida(0);
}

void sobel(IplImage* img, IplImage* out){
    int sm[9] = { 1, 0, -1, 2, 0, -2, 1, 0, -1};
    int sm2[9] = { 1, 2, 1, 0, 0, 0, -1, -2, -1};
    int temp =0, temp2 =0;
    
    for (int i = 1; i < img->height-1;i++){
        uchar* ptr_img = (uchar*) (img->imageData + (i-1) * img->widthStep);
        uchar* ptr_img1 = (uchar*) (img->imageData + i * img->widthStep);
        uchar* ptr_img2 =     (uchar*) (img->imageData + (i+1) * img->widthStep);
        
        uchar* ptr_out = (uchar*) (out->imageData + (i-1) * out->widthStep);
        for (int j= 1; j< img->width-1; j++){
            temp =  sm[0] * ptr_img[j-1]  + sm[1] * ptr_img[j]  + sm[2] * ptr_img[j+1]  +
                    sm[3] * ptr_img1[j-1] + sm[4] * ptr_img1[j] + sm[5] * ptr_img1[j+1] +
                    sm[6] * ptr_img2[j-1] + sm[7] * ptr_img2[j] + sm[8] * ptr_img2[j+1];
            
            temp2 += sm2[0] * ptr_img[j-1] + sm2[1] * ptr_img[j]+ sm2[2] * ptr_img[j+1] +
                     sm2[3] * ptr_img1[j-1]+ sm2[4] * ptr_img1[j]+ sm2[5] * ptr_img1[j+1]+
                     sm2[6] * ptr_img2[j-1]+ sm2[7] * ptr_img2[j]+ sm2[8] * ptr_img2[j+1];
            
            ptr_out[j-1]= int (round(sqrt((temp * temp) + (temp2 * temp2))));
            temp = 0; temp2 =0;
        }
    }
}

void validarSobel(int argc, char* argv []){
    carregarImg(argv, 2, argc);
    lerNomeSaida(argv, 3, argc);
    criarImgSaida(2);
}

int validarGaussiana(int argc, char* argv []){
    carregarImg(argv, 4, argc);
    lerNomeSaida(argv, 5, argc);
    string tam = argv[2];
    int tamanho = atoi(tam.c_str());
    if (tamanho % 2 == 0 ) {
        cout << " -- Tamanho da mascara inválido" << endl;
        exit(0);
    }
    criarImgSaida(3, tamanho);
    return tamanho;
}

void gaussiana(IplImage* img, IplImage* out, int tam, int des ){
    int kernelS = tam;
    double desvio = des;
    double kernel[kernelS][kernelS];
    double media = kernelS/2;
    double soma = 0.0;
    for (int x = 0; x < kernelS; ++x)
        for (int y = 0; y < kernelS; ++y) {
            kernel[x][y] = exp( -0.5 * (pow((x-media)/desvio, 2.0) + pow((y-media)/desvio,2.0)) )
            / (2 * M_PI * desvio * desvio);
            
            soma += kernel[x][y];
        }
    
    // Normaliza o kernel
    for (int x = 0; x < kernelS; ++x)
        for (int y = 0; y < kernelS; ++y)
            kernel[x][y] /= soma;
    
    double temp=0;
    int raio = (kernelS-1)/2;
    
    for (int i = 0; i < img->height-raio-1;i++){
        uchar* ptr_out = (uchar*) (out->imageData + i * out->widthStep);
        for (int j= 0; j< img->width-raio-1; j++){
            for(int k= 0; k<kernelS; ++k){
                uchar* ptr_img = (uchar*) (img->imageData + (i+k) * img->widthStep);
                
                for(int l= 0; l<kernelS; ++l){
                    temp = ptr_img[j+l]*kernel[l][k]+temp;
                }
            }
            ptr_out[j] = temp;
            temp = 0;
            
        }
    }
}

void lerEntrada(int argc, char* argv []){
    if (argc <= 1) {
        cout << " -- Entrada invalida" << endl;
        exit(0);
    }
    op = argv[1];
    if (op.compare("negativo") == 0){
        validarNegativo(argc, argv);
        negativo(imgIn, imgOut);
    }else
        if (op.compare("limiarizacao") == 0) {
            int aux = validarLimiarizacao(argc, argv);
            limiarizar(imgIn, imgOut, aux);
        }else
            if (op.compare("gaussiana") == 0) {
                int tamanhoMas = validarGaussiana(argc, argv);
                string des = argv[3];
                int desvio = atoi(des.c_str());
                gaussiana(imgIn, imgOut, tamanhoMas, desvio);
            }else
                if (op.compare("sobel") == 0) {
                    validarSobel(argc, argv);
                    sobel(imgIn,imgOut);
                }else
                    if (op.compare("histograma") == 0) {
                        string nomeTexto = validarHistograma(argc, argv);
                        ofstream outfile (nomeTexto);
                        histograma(imgIn, imgOut, outfile);
                    }else
                        if (op.compare("equalizacao") == 0) {
                            validarEqualizacao(argc, argv);
                            equalizacao(imgIn, imgOut);
                        }else{
                            cout << " -- Opcao invalida" << endl;
                            exit(0);
                        }
}

int main (int argc, char* argv []){
    lerEntrada(argc, argv);

    //cria duas janelas
    cvNamedWindow( "Imagem Original", CV_WINDOW_AUTOSIZE);
    cvNamedWindow( "Imagem de Saida", CV_WINDOW_AUTOSIZE);
    
    cvSaveImage(nomeSaida.c_str(), imgOut);
    
    //exibe a imagem img na janela
    cvShowImage( "Imagem Original", imgIn );
    
    //exibe a imagem out na janela
    cvShowImage( "Imagem de Saida", imgOut );
    
    //faz com que o programa espere por um evento do teclado
    cvWaitKey(0);
    
    //destroi janelas
    cvDestroyWindow( "Imagem Original");
    cvDestroyWindow( "Imagem de Saida");
    
    //destroi imagem
    cvReleaseImage( &imgIn );
    cvReleaseImage( &imgOut );
    
    return 1;
}
